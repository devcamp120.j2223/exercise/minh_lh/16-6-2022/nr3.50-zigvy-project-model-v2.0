// import express
const express = require('express');
// import mongoose
const mongoose = require('mongoose');
// tạo cổng
const port = 8000;
// khởi tạo app
const app = express();
// khởi tạo json
app.use(express.json());
// chạy trên unicode
app.use(express.urlencoded({
    extended: true
}));
// chạy mongoose
mongoose.connect("mongodb://localhost:27017/123", (error) => {
    if (error) {
        throw error
    }
    console.log("mongodb successfully established")
});
// console cổng
app.listen(port, () => {
    console.log('listening on port ' + port);
});